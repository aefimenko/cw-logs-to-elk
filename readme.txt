Required dependencies:
- Docker
- Elasticsearch (install with "docker pull elasticsearch")
- Logstash (install with "docker pull kibana")
- Kibana (install with "docker pull logstash"). 
What is it: In this project we use Logstash with input plugin "Logstash Input for CloudWatch Logs", which get logs from CW using AWS SDK. Then logs transfer to Elasticsearch. Kibana used for convenient display of the logs. 
For run container:
1. Set LOG_GROUPS, AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY enviroment variables (after that execute "docker-compose config") or edit docker-compose.yml file.
2. Execute "docker-compose build", this command install needed Logstash plugins and set config for container.
3. Execute "docker-compose up cloud_watch_logs" for starting container.
4. Go to http://localhost:5601 add or set index pattern "cw_logs-*" and watch logs!
WARNING: When you search logs by group name or other fields that can contain special symbols as "/", you must shielding this symbols.
Example: if you need to get all logs for group with name "aws/lambda/example-func" you should type "aws//lambda//example-func"
Explanation for some fields:
cloudwatch_logs.log_group - group from AWS;
cloudwatch_logs.ingestion_time  - is the time CloudWatch actually received the message about the event.