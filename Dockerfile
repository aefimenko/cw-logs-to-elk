FROM logstash

RUN logstash-plugin install logstash-input-cloudwatch_logs
RUN logstash-plugin install logstash-output-elasticsearch
COPY logstash.conf /etc/logstash/
CMD ["-f", "/etc/logstash/logstash.conf"]